# What things do

Some useful files are listed below:

| Source file          | Description       |
| -------------------- | ----------------- |
| config/auth.js       | spotify secrets   |
| components/Header.js | top heading       |
| pages/\_document.js  | the starting page |

# TODO

- Test contribution with others
- Deploy to S3 with Bitbucket
- Add google analytics
- Ability to load existing playlists

# Limitations

Once changes are made on spotify, it seems to prevent playing from
