FROM node

COPY . /app
WORKDIR /app
RUN npm run build 

ENTRYPOINT npm run start
