# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Change log and contributing tips

### Changed

- pages/\_document.js
  - switch to `group bangers: only the best, democratically selected`
- components/Header.js
  - logo removed and switched to plain text `Group Bangers`
- static/c-icon\*
  - put silly logos

## Removed

- About link was taken out because it is throwing an error page.
- Original google analytics was taken out so it can be replaced in future when deployed.
